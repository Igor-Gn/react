import React from "react";
import ReactDOM from "react-dom";
import data from "./data";

function App() {
    return (
        <div>
            <h1>Курс валют</h1>

            <ul>
                <li>{data[0].ccy}</li>
                <li>{data[1].ccy}</li>
            </ul>
        </div>

    )
}

ReactDOM.render(<App />, document.getElementById('body'));
console.dir(data)
